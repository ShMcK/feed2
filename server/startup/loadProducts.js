+Meteor.startup(function () {
  if (Products.find().count() === 0) {

    var products = [
      {
        "date": "2015-05-22",
        "type": "web",
        "title": "Redacted",
        "heading": "Quick and easy to redact parts of an image",
        "user": "shmck",
        "links": {
          "web": "http://www.redactapp.com/",
          "apple": "https://itunes.apple.com/us/app/redacted/id984968384?mt=12"
        },
        "votes": [],
        "comments": [{
          owner: 'Shawn McKay',
          username: 'shmck',
          comment: 'An example comment',
          createdAt: new Date(),
          votes: 0
        }, {
          owner: 'Wenj',
          username: 'genre5',
          comment: 'Another comment',
          createdAt: new Date(),
          votes: 0
        }]
      },
      {
        "date": "2015-05-22",
        "type": "web",
        "title": "Tailor",
        "heading": "Show each visitor the best possible landing page",
        "links": {
          "web": "http://www.baynote.com/landing-page-optimization/"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-23",
        "type": "music",
        "title": "Touch Pianist",
        "heading": "Play piano like a pro without playing piano",
        "links": {
          "apple": "https://itunes.apple.com/us/app/touch-piano!-free/id359012799?mt=8"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-23",
        "type": "web",
        "title": "Sunshine",
        "heading": "Instant file sharing without cloud storage",
        "links": {
          "android": "https://play.google.com/store/apps/details?id=com.spika.funki.app"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-23",
        "type": "music",
        "title": "Grooveshark.io",
        "heading": "Music streaming service, Grooveshark, is back from the dead",
        "links": {
          "web": "http://grooveshark.io"
        },
        "votes": [],
        "comments": []
      }, {
        "date": "2015-05-23",
        "type": "web",
        "title": "Redacted",
        "heading": "Quick and easy to redact parts of an image",
        "user": "shmck",
        "links": {
          "web": "http://www.redactapp.com/",
          "apple": "https://itunes.apple.com/us/app/redacted/id984968384?mt=12"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-23",
        "type": "web",
        "title": "Tailor",
        "heading": "Show each visitor the best possible landing page",
        "links": {
          "web": "http://www.baynote.com/landing-page-optimization/"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-24",
        "type": "music",
        "title": "Touch Pianist",
        "heading": "Play piano like a pro without playing piano",
        "links": {
          "apple": "https://itunes.apple.com/us/app/touch-piano!-free/id359012799?mt=8"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-24",
        "type": "web",
        "title": "Sunshine",
        "heading": "Instant file sharing without cloud storage",
        "links": {
          "android": "https://play.google.com/store/apps/details?id=com.spika.funki.app"
        },
        "votes": [],
        "comments": []
      },
      {
        "date": "2015-05-24",
        "type": "music",
        "title": "Grooveshark.io",
        "heading": "Music streaming service, Grooveshark, is back from the dead",
        "links": {
          "web": "http://grooveshark.io"
        },
        "votes": [],
        "comments": []
      }
    ];

    // add products
    for (var i = 0; i < products.length; i++) {
      Products.insert(products[i]);
    }
  }
});