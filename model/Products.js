Products = new Mongo.Collection('products');

//Products.allow({
//  insert: function (product) {
//    var currentUserId = Meteor.userId();
//    return currentUserId && product.owner === currentUserId;
//  },
//  update: function (product, fields, modifier) {
//    var currentUserId = Meteor.userId();
//    return currentUserId === product.owner;
//  },
//  remove: function (product) {
//    var currentUserId = Meteor.userId();
//    return currentUserId === product.owner;
//  }
//});

Meteor.publish('products', function (options, searchString) {
  if (searchString == null) {
    searchString = '';
  }

  Counts.publish(this, 'numberOfProducts', Products.find({
    'heading': {'$regex': '.*' + searchString || '' + '.*', '$options': 'i'}
  }), {noReady: true});

  return Products.find({
    'heading': {'$regex': '.*' + searchString || '' + '.*', '$options': 'i'}
  }, options);
});

Meteor.methods({

  'addProduct': function (product) {
    // check if user is adminif(product.isObject)
    if (typeof product !== 'object')
      throw 'Not a valid product';

    product.comments = [];
    product.votes = [];
    console.log(product);
    Products.insert(product);
  },

  'productVote': function (productId) {
    var userId = Meteor.userId();
    if (!userId)
      throw Meteor.Error('Please Login first.');
    Products.update(productId, {$addToSet: {votes: userId}});
  },

  'postComment': function (productId, comment) {
    var userId = Meteor.userId();
    if (!userId)
      throw Meteor.Error('Please Login first.');
    Products.update(productId, {
      $push: {
        comments: {
          owner: Meteor.userId(),
          username: Meteor.user().username,
          comment: comment,
          createdAt: new Date()
        }
      }
    });
  }
});