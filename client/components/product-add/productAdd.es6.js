class AddProductCtrl {
  constructor() {
    this.newProduct = {};
    this.linkItems = ['web', 'apple', 'android', 'chrome'];
    var links = [];
    _.forEach(this.linkItems, function (link) {
      links.push({
        key: link,
        type: 'bb-input',
        templateOptions: {
          label: link,
          type: 'url'
        }
      });
    });
    this.productFields = [{
      key: 'title',
      type: 'bb-input',
      templateOptions: {
        label: 'Title',
        required: true
      }
    }, {
      key: 'header',
      type: 'bb-input',
      templateOptions: {
        label: 'Header',
        required: true
      }
    }, {
      key: 'postDate',
      type: 'bb-input',
      templateOptions: {
        label: 'Post Date',
        required: true
      }
    }, {
      template: '<h1>Links</h1>'
    }];
    this.productFields = this.productFields.concat(links);
  }

  submit(isValid) {
    if (isValid) {
      var product = {links: {}};
      _.each(this.newProduct, (val, key) => {
        if (!val.length) {
          return;
        }
        else if (this.linkItems.indexOf(key) !== -1) {
          console.log('adding ', key);
          product.links[key] = val;
        } else {
          product[key] = val;
        }
      });
      console.log(product);
      Meteor.call('addProduct', product);
    }
  }
}

angular.module('app.products').controller('AddProductCtrl', [AddProductCtrl]);