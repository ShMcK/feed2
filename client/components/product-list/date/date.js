angular.module('app.products').directive('productDate', productDate);

function productDate() {
  return {
    scope: {
      date: '@'
    },
    templateUrl: 'client/components/product-list/date/date.ng.html',
    link: function (scope, element, attrs) {
      // set date with meteor.js to be relative
      scope.relativeDate = moment(attrs.date, "YYYYMMDD").fromNow();
    }
  };
}