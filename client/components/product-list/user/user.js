angular.module('app.products')
  .directive('productUser', productUser);

function productUser() {
  return {
    scope: {
      user: '='
    },
    templateUrl: 'client/components/product-list/user/user.ng.html'
  };
}