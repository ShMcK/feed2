class ProductListCommentsController {
  constructor() {
  }
}
ProductListCommentsController.$inject = [];

angular.module('app.products').directive('productListComments', productListComments);

function productListComments() {
  return {
    scope: {
      comments: '='
    },
    templateUrl: 'client/components/product-list/comments/comments.ng.html',
    controller: ProductListCommentsController,
    controllerAs: 'comments'
  };
}