class VoteController {
  constructor() {
    this.voted = false;

  }

  vote(id) {
    if (!this.voted) {
      this.voted = true;
      Meteor.call('productVote', id);
    }
  }
}
VoteController.$inject = [];

angular.module('app.products').directive('productVote', productVote);

function productVote() {
  return {
    scope: {
      votes: '=',
      id: '@'
    },
    templateUrl: 'client/components/product-list/vote/vote.ng.html',
    controllerAs: 'vote',
    controller: VoteController
  };
}
