angular.module('app.products')
  .directive('productInfo', productInfo);

function productInfo() {
  return {
    scope: {
      links: '=',
      icons: '=',
      title: '@',
      heading: '@'
    },
    templateUrl: 'client/components/product-list/info/info.ng.html',
    controller: function () {
    },
    controllerAs: 'info'
  };
}