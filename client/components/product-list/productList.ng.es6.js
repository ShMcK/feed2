class ProductListCtrl {
  constructor($meteor, $scope) {
    this.page = 1;
    this.perPage = 5;
    this.sort = {date: 1};
    this.orderProperty = 1;
    this.Products = $meteor.collection(() => {
      return Products.find({}, {
        sort: $scope.getReactively('productList.sort')
      });
    });

    $meteor.autorun($scope, function () {
      $meteor.subscribe('products', {
        limit: parseInt($scope.getReactively('productList.perPage')),
        skip: parseInt(($scope.getReactively('productList.page') - 1)) * parseInt($scope.getReactively('productList.perPage')),
        sort: $scope.getReactively('productList.sort')
      }, $scope.getReactively('productList.search'))
        .then(() => {
          this.productsCount = $meteor.object(Counts, 'numberOfProducts', false);
        });
    });

    $scope.$watch('productList.orderProperty', () => {
      if (this.orderProperty)
        this.sort = {date: parseInt(this.orderProperty)};
    });

  }

  showDate(index, date, previousDate) {
    if (index === 0)
      return true;
    return !(date === previousDate);
  }
}

angular.module('app.products')
  .controller('ProductListCtrl', ['$meteor', '$scope', ProductListCtrl]);