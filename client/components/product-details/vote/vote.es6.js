angular.module('app.products')
  .directive('commentVote', commentVote);


/* todo: vote up, vote down, flag, share */

function commentVote() {
  return {
    templateUrl: 'client/components/product-details/vote/vote.ng.html',
    controller: function () {
    },
    controllerAs: 'vote'
  };
}