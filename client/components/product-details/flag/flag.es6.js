angular.module('app.products')
  .directive('commentFlag', commentFlag);

function commentFlag() {
  return {
    templateUrl: 'client/components/product-details/flag/flag.ng.html',
    controller: function () {}
  };
}