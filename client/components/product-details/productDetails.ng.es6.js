class ProductDetailsCtrl {
  constructor($meteor, $stateParams, $scope) {
    this.productId = $stateParams.productId;
    this.product = $meteor.object(Products, {_id: $stateParams.productId});

    var subscriptionHandle;
    $meteor.subscribe('products').then((handle) => {
      subscriptionHandle = handle;
    });

    $scope.$on('$destroy', function () {
      subscriptionHandle.stop();
    });
  }
}

angular.module('app.products')
  .controller('ProductDetailsCtrl', ['$meteor', '$stateParams', '$scope', ProductDetailsCtrl]);