angular.module('app.products')
  .directive('productPostComments', productPostComments);

function productPostComments() {
  return {
    scope: {
      productId: '@'
    },
    templateUrl: 'client/components/product-details/post/post.ng.html',
    controller: function () {
      var post = this;
      post.commentField = [{
        key: 'comment',
        type: 'bb-textarea',
        templateOptions: {
          label: 'Comment'
        }
      }];
    },
    controllerAs: 'post',
    link: function (scope, elem, attrs) {
      scope.submit = () => {
        if (scope.comment.length) {
          Meteor.call('postComment', attrs.productId, scope.comment);
          scope.comment = '';
        }
      }
    }
  };
}