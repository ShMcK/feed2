class ProductCommentsController {
  constructor() {
  }
}
ProductCommentsController.$inject = [];

angular.module('app.products').directive('productComments', productComments);

function productComments() {
  return {
    scope: {
      comments: '='
    },
    templateUrl: 'client/components/product-details/comments/comments.ng.html',
    controller: ProductCommentsController,
    controllerAs: 'productComments'
  };
}