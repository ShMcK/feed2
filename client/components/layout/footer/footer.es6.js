/**
 * Footer using Bourbon refills
 * @returns {{template: (*|any), controllerAs: string, controller: Function}}
 */
angular.module('app.layout').directive('bourbonFooter', bourbonFooter);

function bourbonFooter() {
  return {
    templateUrl: 'client/components/layout/footer/footer.ng.html',
    controllerAs: 'footer',
    controller: function () {
      var data = new Promise((resolve, reject) => {
        HTTP.get('/json/index.json', (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result.data);
          }
        });
      }).then((data) => {
          angular.extend(this, {
            logo: data.footer.logo,
            primary: data.nav.primary,
            secondary: data.footer.secondary,
            social: data.footer.social
          });
        });

    }
  };
}