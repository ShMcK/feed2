/**
 * Navbar using Bourbon Refills
 * @returns {{template: (*|any), controllerAs: string, controller: Function}}
 */
angular.module('app.layout')
  .directive('bourbonNavbar', bourbonNavbar);

function bourbonNavbar() {
  return {
    templateUrl: 'client/components/layout/nav/nav.ng.html',
    controllerAs: 'navbar',
    controller: function () {
      var data = new Promise((resolve, reject) => {
        HTTP.get('/json/index.json', (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result.data);
          }
        });
      }).then((data) => {
          angular.extend(this, {
            title: data.title,
            nav: data.nav
          });
        });


      // bourbon refill nav
      // rewrite for angular
      $(document).ready(function () {
        var menuToggle = $('#js-mobile-menu').unbind();
        $('#js-navigation-menu').removeClass('show');

        menuToggle.on('click', function (e) {
          e.preventDefault();
          $('#js-navigation-menu').slideToggle(function () {
            if ($('#js-navigation-menu').is(':hidden')) {
              $('#js-navigation-menu').removeAttr('style');
            }
          });
        });
      });

    }
  };
}