angular.module('app', [
  'ngAnimate',
  'ngSanitize',
  'ngCookies',
  'ngTouch',

  'angular-meteor',
  'formly',
  'formlyBourbon',
  'ui.router',
  'pascalprecht.translate',
  'infinite-scroll',

  // modules
  'app.layout',
  'app.products',
  'app.profile'
]);

function onReady() {
  angular.bootstrap(document, ['app']);
}

if (Meteor.isCordova) {
  angular.element(document).on('deviceReady', onReady);
} else {
  angular.element(document).ready(onReady);
}


