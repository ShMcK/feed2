/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	(function () {
	  'use strict';
	  var MODULE_NAME = 'formlyBourbon';
	  var PREFIX = 'bb';
	  var USING_TEMPLATES = true;
	  var USING_TEMPLATE_VALIDATION = true;
	  /* Custom validation message defaults here */
	  var VALIDATION_MESSAGES = [{
	    name: 'required', message: 'This field is required'
	  }, {
	    name: 'maxlength', message: 'This field is too long.'
	  }, {
	    name: 'minlength', message: 'This field is too short.'
	  }];
	  /* Module Templates + Data */
	  var FIELDS = [];
	  var fields = ['checkbox', 'input', 'radio', 'select', 'textarea', 'switch'];
	  fields.map(function (field) {
	    FIELDS.push({
	      name: field,
	      template: __webpack_require__(1)("./" + field + '.html')
	    });
	  });
	
	  function _prefixer(name) {
	    return PREFIX + '-' + name;
	  }
	
	  function _fieldTemplateUrl(name) {
	    return 'fields/formly-fields-' + _prefixer(name) + '.html';
	  }
	
	  /*@ngInject*/
	  function setFields(formlyConfig, formlyApiCheck) {
	    if (USING_TEMPLATES) {
	      if (USING_TEMPLATE_VALIDATION) {
	        /* validate options using apiCheck to reduce developer errors */
	        FIELDS.map(function (field) {
	          formlyConfig.setType({
	            name: _prefixer(field.name),
	            templateUrl: _fieldTemplateUrl(field.name),
	            validateOptions: function validateOptions(options) {
	              options.data.apiCheck = formlyApiCheck.warn(formlyApiCheck.shape({ templateOptions: formlyApiCheck.shape(field.templateOptions || {}).optional }), arguments);
	            }
	          });
	        });
	      } else {
	        /* skip validating options */
	        //apiCheck.disable();
	        FIELDS.map(function (field) {
	          formlyConfig.setType({ name: _prefixer(field.name), templateUrl: _fieldTemplateUrl(field.name) });
	        });
	      }
	    }
	  }
	  setFields.$inject = ["formlyConfig", "formlyApiCheck"];
	
	  function setDefaults(formlyConfig, formlyValidationMessages) {
	    formlyConfig.extras.ngModelAttrsManipulatorPreferBound = true;
	    VALIDATION_MESSAGES.map(function (validation) {
	      formlyValidationMessages.addStringMessage(validation.name, validation.message);
	    });
	    formlyValidationMessages.messages.pattern = function (viewValue, modelValue) {
	      return (viewValue || modelValue) + ' is invalid';
	    };
	  }
	  setDefaults.$inject = ["formlyConfig", "formlyValidationMessages"];
	
	  function cacheTemplates($templateCache) {
	    if (USING_TEMPLATES) {
	      FIELDS.map(function (field) {
	        $templateCache.put(_fieldTemplateUrl(field.name), field.template);
	      });
	    }
	  }
	  cacheTemplates.$inject = ["$templateCache"];
	
	  // load select
	  __webpack_require__(2);
	  angular.module(MODULE_NAME, ['formly', __webpack_require__(3).name]).run(setFields).run(setDefaults).run(cacheTemplates);
	})();

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./checkbox.html": 4,
		"./input.html": 5,
		"./radio.html": 6,
		"./select.html": 7,
		"./switch.html": 8,
		"./textarea.html": 9
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 1;


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/* dropdown select */
	'use strict';
	
	$(document).ready(function () {
	  $('.dropdown-button').click(function () {
	    $('.dropdown-menu').toggleClass('show-menu');
	    $('.dropdown-menu > li').click(function () {
	      $('.dropdown-menu').removeClass('show-menu');
	    });
	    $('.dropdown-menu.dropdown-select > li').click(function () {
	      $('.dropdown-button').html($(this).html());
	    });
	  });
	});

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/* global angular */
	'use strict';Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	// jshint ignore:line
	
	exports['default'] = angular.module('lumx.text-field', []).filter('unsafe', ['$sce', function ($sce) {
	  return $sce.trustAsHtml;
	}]).directive('lxTextField', ['$timeout', function ($timeout) {
	  return {
	    restrict: 'E',
	    scope: {
	      label: '@',
	      disabled: '&',
	      error: '&',
	      valid: '&',
	      fixedLabel: '&',
	      icon: '@',
	      theme: '@'
	    },
	    template: __webpack_require__(10),
	    replace: true,
	    transclude: true,
	    link: function link(scope, element, attrs, ctrl, transclude) {
	      if (angular.isUndefined(scope.theme)) {
	        scope.theme = 'light';
	      }
	
	      var modelController, $field;
	
	      scope.data = {
	        focused: false,
	        model: undefined
	      };
	
	      function focusUpdate() {
	        scope.data.focused = true;
	        scope.$apply();
	      }
	
	      function blurUpdate() {
	        scope.data.focused = false;
	        scope.$apply();
	      }
	
	      function modelUpdate() {
	        scope.data.model = modelController.$modelValue || $field.val();
	      }
	
	      function valueUpdate() {
	        modelUpdate();
	        scope.$apply();
	      }
	
	      function updateTextareaHeight() {
	        $timeout(function () {
	          $field.removeAttr('style').css({ height: $field[0].scrollHeight + 'px' });
	        });
	      }
	
	      transclude(function () {
	        $field = element.find('textarea');
	
	        if ($field[0]) {
	          updateTextareaHeight();
	
	          $field.on('cut paste drop keydown', function () {
	            updateTextareaHeight();
	          });
	        } else {
	          $field = element.find('input');
	        }
	
	        $field.addClass('text-field__input');
	        $field.on('focus', focusUpdate);
	        $field.on('blur', blurUpdate);
	        $field.on('propertychange change click keyup input paste', valueUpdate);
	
	        modelController = $field.data('$ngModelController');
	
	        scope.$watch(function () {
	          return modelController.$modelValue;
	        }, modelUpdate);
	      });
	    }
	  };
	}]);
	module.exports = exports['default'];

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div class=\"checkbox\">\n  <input ng-model=\"model[options.key]\"\n         type=\"checkbox\"\n         role=\"checkbox\"\n         class=\"checkbox__input\">\n  <label for={{::id}} class=\"checkbox__label\" aria-label=\"{{::to.label}}\">{{to.label}}</label>\n\t<span ng-if=\"::to.description\"\n        class=\"checkbox__help\">{{::to.description}}</span>\n</div>"

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<lx-text-field model=\"::model[options.key]\"\n               icon=\"{{::to.icon}}\"\n               fixed-label=\"::to.fixedLabel\"\n               theme=\"{{::to.theme}}\"\n               disabled=\"to.disabled\"\n               label=\"{{to.label}} {{::to.required ? '*' : ''}}\"\n               valid=\"fc.$valid && fc.$touched\"\n               error=\"fc.$invalid && fc.$touched\">\n  <input ng-model=\"model[options.key]\"\n         type=\"{{::to.type}}\"\n         aria-label=\"{{::to.label}}\"\n         ng-class=\"::to.className\"/>\n</lx-text-field>"

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div class=\"radio-group\">\n  <h3 ng-if=\"::to.label\"><label>{{::to.label}}</label></h3>\n\n  <div class=\"radio-button\"\n       ng-class=\"{'radio-button__inline': to.inline}\"\n       ng-repeat=\"o in to.options\">\n    <input ng-model=\"$parent.model[$parent.options.key]\"\n           id=\"{{::id + '_' + $index}}\"\n           type=\"radio\"\n           ng-disabled=\"::o.disabled\"\n           class=\"radio-button__input\"\n           ng-value=\"::o.value\"\n           aria-labelledby=\"{{::id + '_' + $index + '_radioButton'}}\">\n    <label for=\"{{::id + '_' + $index}}\"\n           class=\"radio-button__label\">{{::o.name}}</label>\n      <span ng-if=\"::o.description\" class=\"radio-button__help\">{{::o.description}}\n      </span>\n  </div>\n</div>\n"

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div class=\"dropdown\">\n  <div class=\"dropdown-container\">\n    <p class=\"dropdown-description\">{{::to.label}}</p>\n    <p class=\"dropdown-button\">Click to Select</p>\n    <ul class=\"dropdown-menu dropdown-select\">\n      <li ng-repeat=\"option in to.options\">{{::option}}</li>\n    </ul>\n  </div>\n</div>"

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<label class=\"label-switch\">\n  <input type=\"checkbox\"\n         role=\"checkbox\"\n         ng-model=\"model[options.key]\"/>\n  <div class=\"checkbox\"></div>\n  {{to.label}}\n  <span ng-if=\"::to.description\"\n        class=\"checkbox__help\">{{::to.description}}</span>\n</label>"

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<lx-text-field model=\"::model[options.key]\"\n               fixed-label=\"::to.fixedLabel\"\n               icon=\"{{::to.icon}}\"\n               theme=\"{{::to.theme}}\"\n               label=\"{{to.label}} {{::to.required ? '*' : ''}}\"\n               valid=\"fc.$valid && fc.$touched\"\n               error=\"fc.$invalid && fc.$touched\">\n    <textarea ng-model=\"model[options.key]\"\n              aria-label=\"{{::to.label}}\"\n              rows=\"{{::to.rows}}\" cols=\"{{::to.cols}}\">\n    </textarea>\n</lx-text-field>\n"

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = "<div class=\"text-field text-field--{{ theme }}-theme\"\n     ng-class=\"{ 'text-field--is-valid': valid(),\n                 'text-field--has-error': error(),\n                 'text-field--is-disabled': disabled(),\n                 'text-field--fixed-label': fixedLabel(),\n                 'text-field--is-active': data.model || data.focused,\n                 'text-field--is-focused': data.focused,\n                 'text-field--label-hidden': fixedLabel() && data.model,\n                 'text-field--with-icon': icon && fixedLabel() }\">\n  <label class=\"text-field__label\" ng-bind-html=\"label | unsafe\"></label>\n\n  <div class=\"text-field__icon\" ng-if=\"icon && fixedLabel() \">\n    <i class=\"mdi mdi-{{ icon }}\"></i>\n  </div>\n\n  <div ng-transclude=\"1\"></div>\n</div>"

/***/ }
/******/ ]);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgN2JkNGExODkwOWU1YWE2ZGM0MmUiLCJ3ZWJwYWNrOi8vLy4vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vZmllbGRzIF5cXC5cXC8uKlxcLmh0bWwkIiwid2VicGFjazovLy8uL2V4dHJhL3NlbGVjdC5qcyIsIndlYnBhY2s6Ly8vLi9leHRyYS90ZXh0LWZpZWxkLmpzIiwid2VicGFjazovLy8uL2ZpZWxkcy9jaGVja2JveC5odG1sIiwid2VicGFjazovLy8uL2ZpZWxkcy9pbnB1dC5odG1sIiwid2VicGFjazovLy8uL2ZpZWxkcy9yYWRpby5odG1sIiwid2VicGFjazovLy8uL2ZpZWxkcy9zZWxlY3QuaHRtbCIsIndlYnBhY2s6Ly8vLi9maWVsZHMvc3dpdGNoLmh0bWwiLCJ3ZWJwYWNrOi8vLy4vZmllbGRzL3RleHRhcmVhLmh0bWwiLCJ3ZWJwYWNrOi8vLy4vZXh0cmEvdGV4dC1maWVsZC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx1QkFBZTtBQUNmO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7QUN0Q0MsY0FBWTtBQUNYLGVBQVksQ0FBQztBQUNiLE9BQUksV0FBVyxHQUFHLGVBQWUsQ0FBQztBQUNsQyxPQUFJLE1BQU0sR0FBRyxJQUFJLENBQUM7QUFDbEIsT0FBSSxlQUFlLEdBQUcsSUFBSSxDQUFDO0FBQzNCLE9BQUkseUJBQXlCLEdBQUcsSUFBSSxDQUFDOztBQUVyQyxPQUFJLG1CQUFtQixHQUFHLENBQUM7QUFDekIsU0FBSSxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsd0JBQXdCO0lBQ3BELEVBQUU7QUFDRCxTQUFJLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSx5QkFBeUI7SUFDdEQsRUFBRTtBQUNELFNBQUksRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLDBCQUEwQjtJQUN2RCxDQUFDLENBQUM7O0FBRUgsT0FBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO0FBQ2hCLE9BQUksTUFBTSxHQUFHLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztBQUM1RSxTQUFNLENBQUMsR0FBRyxDQUFDLFVBQVMsS0FBSyxFQUFFO0FBQ3pCLFdBQU0sQ0FBQyxJQUFJLENBQUM7QUFDVixXQUFJLEVBQUUsS0FBSztBQUNYLGVBQVEsRUFBRSwyQkFBbUIsR0FBRyxLQUFLLEdBQUcsT0FBTyxDQUFDO01BQ2pELENBQUMsQ0FBQztJQUNKLENBQUMsQ0FBQzs7QUFFSCxZQUFTLFNBQVMsQ0FBQyxJQUFJLEVBQUU7QUFDdkIsWUFBTyxNQUFNLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQztJQUM1Qjs7QUFFRCxZQUFTLGlCQUFpQixDQUFDLElBQUksRUFBRTtBQUMvQixZQUFPLHVCQUF1QixHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7SUFDNUQ7OztBQUdELFlBQVMsU0FBUyxDQUFDLFlBQVksRUFBRSxjQUFjLEVBQUU7QUFDL0MsU0FBSSxlQUFlLEVBQUU7QUFDbkIsV0FBSSx5QkFBeUIsRUFBRTs7QUFDN0IsZUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUMxQix1QkFBWSxDQUFDLE9BQU8sQ0FBQztBQUNuQixpQkFBSSxFQUFFLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO0FBQzNCLHdCQUFXLEVBQUUsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztBQUMxQyw0QkFBZSxFQUFFLHlCQUFVLE9BQU8sRUFBRTtBQUNsQyxzQkFBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUM5RCxFQUFDLGVBQWUsRUFBRSxjQUFjLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxlQUFlLElBQUksRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFDLENBQzlFLEVBQUUsU0FBUyxDQUFDLENBQUM7Y0FDZjtZQUNGLENBQUMsQ0FBQztVQUNKLENBQUMsQ0FBQztRQUNKLE1BQU07OztBQUVMLGVBQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDMUIsdUJBQVksQ0FBQyxPQUFPLENBQUMsRUFBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxXQUFXLEVBQUUsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFDLENBQUMsQ0FBQztVQUNqRyxDQUFDLENBQUM7UUFDSjtNQUNGO0lBQ0Y7O0FBRTREO0FBQzNELGlCQUFZLENBQUMsTUFBTSxDQUFDLGtDQUFrQyxHQUFHLEdBQUs7QUFDOUQsd0JBQW1CLENBQUMsR0FBRyxDQUFDLFVBQVUsVUFBVSxFQUFFO0FBQzVDLCtCQUF3QixDQUFDLGdCQUFnQixDQUFDLEdBQXFDO01BQ2hGLENBQUMsQ0FBQztBQUNILFFBQTZFO0FBQzNFLGNBQU8sQ0FBQyxTQUFTLElBQUksVUFBVSxJQUFJLGFBQWEsQ0FBQztNQUNsRCxDQUFDO0lBQ0g7O0FBRUQsWUFBUyxjQUFjLENBQUMsY0FBYyxFQUFFO0FBQ2pCO0FBQ25CLGFBQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDMUIsdUJBQWMsQ0FBQyxHQUFtRDtRQUNuRSxDQUFDLENBQUM7TUFDSjtJQUNGOzs7QUFHRCxVQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztBQUk2QjtFQUN4RCxHQUFFLENBQUU7Ozs7Ozs7OztBQ2hGTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBaUMsdURBQXVEO0FBQ3hGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FDbEJBLEVBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWTtBQUM1QixJQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWTtBQUN0QyxNQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDN0MsTUFBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVk7QUFDekMsUUFBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO01BQzlDLENBQUMsQ0FBQztBQUNILE1BQUMsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxZQUFZO0FBQ3pELFFBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztNQUM1QyxDQUFDLENBQUM7SUFDSixDQUFDLENBQUM7RUFDSixDQUFDLEM7Ozs7Ozs7QUNWRixhQUFZLENBQUM7Ozs7O3NCQUVFLE9BQU8sQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxDQUFDLENBQ2pELE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBUyxJQUFJLEVBQ3hDO0FBQ0UsVUFBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0VBQ3pCLENBQUMsQ0FBQyxDQUNGLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxVQUFVLEVBQUUsVUFBUyxRQUFRLEVBQ3hEO0FBQ0UsVUFBTztBQUNMLGFBQVEsRUFBRSxHQUFHO0FBQ2IsVUFBSyxFQUFFO0FBQ0wsWUFBSyxFQUFFLEdBQUc7QUFDVixlQUFRLEVBQUUsR0FBRztBQUNiLFlBQUssRUFBRSxHQUFHO0FBQ1YsWUFBSyxFQUFFLEdBQUc7QUFDVixpQkFBVSxFQUFFLEdBQUc7QUFDZixXQUFJLEVBQUUsR0FBRztBQUNULFlBQUssRUFBRSxHQUFHO01BQ1g7QUFDRCxhQUFRLEVBQUUsbUJBQU8sQ0FBQyxFQUFtQixDQUFDO0FBQ3RDLFlBQU8sRUFBRSxJQUFJO0FBQ2IsZUFBVSxFQUFFLElBQUk7QUFDaEIsU0FBSSxFQUFFLGNBQVMsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFDdEQ7QUFDRSxXQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUNwQztBQUNFLGNBQUssQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO1FBQ3ZCOztBQUVELFdBQUksZUFBZSxFQUNqQixNQUFNLENBQUM7O0FBRVQsWUFBSyxDQUFDLElBQUksR0FBRztBQUNYLGdCQUFPLEVBQUUsS0FBSztBQUNkLGNBQUssRUFBRSxTQUFTO1FBQ2pCLENBQUM7O0FBRUYsZ0JBQVMsV0FBVyxHQUNwQjtBQUNFLGNBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztBQUMxQixjQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDaEI7O0FBRUQsZ0JBQVMsVUFBVSxHQUNuQjtBQUNFLGNBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztBQUMzQixjQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDaEI7O0FBRUQsZ0JBQVMsV0FBVyxHQUNwQjtBQUNFLGNBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLGVBQWUsQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ2hFOztBQUVELGdCQUFTLFdBQVcsR0FDcEI7QUFDRSxvQkFBVyxFQUFFLENBQUM7QUFDZCxjQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDaEI7O0FBRUQsZ0JBQVMsb0JBQW9CLEdBQzdCO0FBQ0UsaUJBQVEsQ0FBQyxZQUNUO0FBQ0UsaUJBQU0sQ0FDSCxVQUFVLENBQUMsT0FBTyxDQUFDLENBQ25CLEdBQUcsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFHLElBQUksRUFBRSxDQUFDLENBQUM7VUFDbkQsQ0FBQyxDQUFDO1FBQ0o7O0FBRUQsaUJBQVUsQ0FBQyxZQUNYO0FBQ0UsZUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7O0FBRWxDLGFBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUNiO0FBQ0UsK0JBQW9CLEVBQUUsQ0FBQzs7QUFFdkIsaUJBQU0sQ0FBQyxFQUFFLENBQUMsd0JBQXdCLEVBQUUsWUFDcEM7QUFDRSxpQ0FBb0IsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQztVQUNKLE1BRUQ7QUFDRSxpQkFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7VUFDaEM7O0FBRUQsZUFBTSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0FBQ3JDLGVBQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0FBQ2hDLGVBQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0FBQzlCLGVBQU0sQ0FBQyxFQUFFLENBQUMsK0NBQStDLEVBQUUsV0FBVyxDQUFDLENBQUM7O0FBRXhFLHdCQUFlLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOztBQUVwRCxjQUFLLENBQUMsTUFBTSxDQUFDLFlBQ2I7QUFDRSxrQkFBTyxlQUFlLENBQUMsV0FBVyxDQUFDO1VBQ3BDLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDakIsQ0FBQyxDQUFDO01BQ0o7SUFDRixDQUFDO0VBQ0gsQ0FBQyxDQUFDOzs7Ozs7O0FDeEdMLG1NQUFrTSxNQUFNLDBDQUEwQyxZQUFZLEtBQUssVUFBVSxpRkFBaUYsa0JBQWtCLGdCOzs7Ozs7QUNBaFgsMEZBQXlGLFdBQVcsNkVBQTZFLFlBQVksc0VBQXNFLFVBQVUsR0FBRywwQkFBMEIseUtBQXlLLFdBQVcsNEJBQTRCLFlBQVksNkQ7Ozs7OztBQ0F0Z0Isb0ZBQW1GLFlBQVksbUVBQW1FLGtDQUFrQyx5SEFBeUgscUJBQXFCLG9MQUFvTCxzQ0FBc0Msd0JBQXdCLHFCQUFxQiwrQ0FBK0MsVUFBVSwrRUFBK0UsaUJBQWlCLG9DOzs7Ozs7QUNBbHZCLDJIQUEwSCxZQUFZLDZKQUE2SixVQUFVLG1DOzs7Ozs7QUNBN1MsNkxBQTRMLFVBQVUseUVBQXlFLGtCQUFrQixrQjs7Ozs7O0FDQWpTLDBJQUF5SSxXQUFXLDZCQUE2QixZQUFZLDZCQUE2QixVQUFVLEdBQUcsMEJBQTBCLHlMQUF5TCxZQUFZLDJCQUEyQixXQUFXLFlBQVksV0FBVyx5Qzs7Ozs7O0FDQW5nQix5REFBd0QsU0FBUywyQkFBMkIsc2RBQXNkLG1MQUFtTCxRQUFRLCtEIiwiZmlsZSI6ImZvcm1seUx1bXhMaXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pXG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG5cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGV4cG9ydHM6IHt9LFxuIFx0XHRcdGlkOiBtb2R1bGVJZCxcbiBcdFx0XHRsb2FkZWQ6IGZhbHNlXG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmxvYWRlZCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiB3ZWJwYWNrL2Jvb3RzdHJhcCA3YmQ0YTE4OTA5ZTVhYTZkYzQyZVxuICoqLyIsIihmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0JztcbiAgdmFyIE1PRFVMRV9OQU1FID0gJ2Zvcm1seUJvdXJib24nO1xuICB2YXIgUFJFRklYID0gJ2JiJztcbiAgdmFyIFVTSU5HX1RFTVBMQVRFUyA9IHRydWU7XG4gIHZhciBVU0lOR19URU1QTEFURV9WQUxJREFUSU9OID0gdHJ1ZTtcbiAgLyogQ3VzdG9tIHZhbGlkYXRpb24gbWVzc2FnZSBkZWZhdWx0cyBoZXJlICovXG4gIHZhciBWQUxJREFUSU9OX01FU1NBR0VTID0gW3tcbiAgICBuYW1lOiAncmVxdWlyZWQnLCBtZXNzYWdlOiAnVGhpcyBmaWVsZCBpcyByZXF1aXJlZCdcbiAgfSwge1xuICAgIG5hbWU6ICdtYXhsZW5ndGgnLCBtZXNzYWdlOiAnVGhpcyBmaWVsZCBpcyB0b28gbG9uZy4nXG4gIH0sIHtcbiAgICBuYW1lOiAnbWlubGVuZ3RoJywgbWVzc2FnZTogJ1RoaXMgZmllbGQgaXMgdG9vIHNob3J0LidcbiAgfV07XG4gIC8qIE1vZHVsZSBUZW1wbGF0ZXMgKyBEYXRhICovXG4gIHZhciBGSUVMRFMgPSBbXTtcbiAgdmFyIGZpZWxkcyA9IFsnY2hlY2tib3gnLCAnaW5wdXQnLCAncmFkaW8nLCAnc2VsZWN0JywgJ3RleHRhcmVhJywgJ3N3aXRjaCddO1xuICBmaWVsZHMubWFwKGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgRklFTERTLnB1c2goe1xuICAgICAgbmFtZTogZmllbGQsXG4gICAgICB0ZW1wbGF0ZTogcmVxdWlyZSgnLi9maWVsZHMvJyArIGZpZWxkICsgJy5odG1sJylcbiAgICB9KTtcbiAgfSk7XG5cbiAgZnVuY3Rpb24gX3ByZWZpeGVyKG5hbWUpIHtcbiAgICByZXR1cm4gUFJFRklYICsgJy0nICsgbmFtZTtcbiAgfVxuXG4gIGZ1bmN0aW9uIF9maWVsZFRlbXBsYXRlVXJsKG5hbWUpIHtcbiAgICByZXR1cm4gJ2ZpZWxkcy9mb3JtbHktZmllbGRzLScgKyBfcHJlZml4ZXIobmFtZSkgKyAnLmh0bWwnO1xuICB9XG5cbiAgLypAbmdJbmplY3QqL1xuICBmdW5jdGlvbiBzZXRGaWVsZHMoZm9ybWx5Q29uZmlnLCBmb3JtbHlBcGlDaGVjaykge1xuICAgIGlmIChVU0lOR19URU1QTEFURVMpIHtcbiAgICAgIGlmIChVU0lOR19URU1QTEFURV9WQUxJREFUSU9OKSB7ICAgICAgICAvKiB2YWxpZGF0ZSBvcHRpb25zIHVzaW5nIGFwaUNoZWNrIHRvIHJlZHVjZSBkZXZlbG9wZXIgZXJyb3JzICovXG4gICAgICAgIEZJRUxEUy5tYXAoZnVuY3Rpb24gKGZpZWxkKSB7XG4gICAgICAgICAgZm9ybWx5Q29uZmlnLnNldFR5cGUoe1xuICAgICAgICAgICAgbmFtZTogX3ByZWZpeGVyKGZpZWxkLm5hbWUpLFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IF9maWVsZFRlbXBsYXRlVXJsKGZpZWxkLm5hbWUpLFxuICAgICAgICAgICAgdmFsaWRhdGVPcHRpb25zOiBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgICAgICAgICBvcHRpb25zLmRhdGEuYXBpQ2hlY2sgPSBmb3JtbHlBcGlDaGVjay53YXJuKGZvcm1seUFwaUNoZWNrLnNoYXBlKFxuICAgICAgICAgICAgICAgIHt0ZW1wbGF0ZU9wdGlvbnM6IGZvcm1seUFwaUNoZWNrLnNoYXBlKGZpZWxkLnRlbXBsYXRlT3B0aW9ucyB8fCB7fSkub3B0aW9uYWx9XG4gICAgICAgICAgICAgICksIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHsgICAgICAgIC8qIHNraXAgdmFsaWRhdGluZyBvcHRpb25zICovXG4gICAgICAgIC8vYXBpQ2hlY2suZGlzYWJsZSgpO1xuICAgICAgICBGSUVMRFMubWFwKGZ1bmN0aW9uIChmaWVsZCkge1xuICAgICAgICAgIGZvcm1seUNvbmZpZy5zZXRUeXBlKHtuYW1lOiBfcHJlZml4ZXIoZmllbGQubmFtZSksIHRlbXBsYXRlVXJsOiBfZmllbGRUZW1wbGF0ZVVybChmaWVsZC5uYW1lKX0pO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBzZXREZWZhdWx0cyhmb3JtbHlDb25maWcsIGZvcm1seVZhbGlkYXRpb25NZXNzYWdlcykge1xuICAgIGZvcm1seUNvbmZpZy5leHRyYXMubmdNb2RlbEF0dHJzTWFuaXB1bGF0b3JQcmVmZXJCb3VuZCA9IHRydWU7XG4gICAgVkFMSURBVElPTl9NRVNTQUdFUy5tYXAoZnVuY3Rpb24gKHZhbGlkYXRpb24pIHtcbiAgICAgIGZvcm1seVZhbGlkYXRpb25NZXNzYWdlcy5hZGRTdHJpbmdNZXNzYWdlKHZhbGlkYXRpb24ubmFtZSwgdmFsaWRhdGlvbi5tZXNzYWdlKTtcbiAgICB9KTtcbiAgICBmb3JtbHlWYWxpZGF0aW9uTWVzc2FnZXMubWVzc2FnZXMucGF0dGVybiA9IGZ1bmN0aW9uICh2aWV3VmFsdWUsIG1vZGVsVmFsdWUpIHtcbiAgICAgIHJldHVybiAodmlld1ZhbHVlIHx8IG1vZGVsVmFsdWUpICsgJyBpcyBpbnZhbGlkJztcbiAgICB9O1xuICB9XG5cbiAgZnVuY3Rpb24gY2FjaGVUZW1wbGF0ZXMoJHRlbXBsYXRlQ2FjaGUpIHtcbiAgICBpZiAoVVNJTkdfVEVNUExBVEVTKSB7XG4gICAgICBGSUVMRFMubWFwKGZ1bmN0aW9uIChmaWVsZCkge1xuICAgICAgICAkdGVtcGxhdGVDYWNoZS5wdXQoX2ZpZWxkVGVtcGxhdGVVcmwoZmllbGQubmFtZSksIGZpZWxkLnRlbXBsYXRlKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIC8vIGxvYWQgc2VsZWN0XG4gIHJlcXVpcmUoJy4vZXh0cmEvc2VsZWN0Jyk7XG4gIGFuZ3VsYXIubW9kdWxlKE1PRFVMRV9OQU1FLCBbXG4gICAgJ2Zvcm1seScsXG4gICAgcmVxdWlyZSgnLi9leHRyYS90ZXh0LWZpZWxkJykubmFtZVxuICBdKS5ydW4oc2V0RmllbGRzKS5ydW4oc2V0RGVmYXVsdHMpLnJ1bihjYWNoZVRlbXBsYXRlcyk7XG59KCkpO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9qc2hpbnQtbG9hZGVyIS4vaW5kZXguanNcbiAqKi8iLCJ2YXIgbWFwID0ge1xuXHRcIi4vY2hlY2tib3guaHRtbFwiOiA0LFxuXHRcIi4vaW5wdXQuaHRtbFwiOiA1LFxuXHRcIi4vcmFkaW8uaHRtbFwiOiA2LFxuXHRcIi4vc2VsZWN0Lmh0bWxcIjogNyxcblx0XCIuL3N3aXRjaC5odG1sXCI6IDgsXG5cdFwiLi90ZXh0YXJlYS5odG1sXCI6IDlcbn07XG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18od2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkpO1xufTtcbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0cmV0dXJuIG1hcFtyZXFdIHx8IChmdW5jdGlvbigpIHsgdGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJy5cIikgfSgpKTtcbn07XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gMTtcblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9maWVsZHMgXlxcLlxcLy4qXFwuaHRtbCRcbiAqKiBtb2R1bGUgaWQgPSAxXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCIvKiBkcm9wZG93biBzZWxlY3QgKi9cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgJCgnLmRyb3Bkb3duLWJ1dHRvbicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAkKCcuZHJvcGRvd24tbWVudScpLnRvZ2dsZUNsYXNzKCdzaG93LW1lbnUnKTtcbiAgICAkKCcuZHJvcGRvd24tbWVudSA+IGxpJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgJCgnLmRyb3Bkb3duLW1lbnUnKS5yZW1vdmVDbGFzcygnc2hvdy1tZW51Jyk7XG4gICAgfSk7XG4gICAgJCgnLmRyb3Bkb3duLW1lbnUuZHJvcGRvd24tc2VsZWN0ID4gbGknKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAkKCcuZHJvcGRvd24tYnV0dG9uJykuaHRtbCgkKHRoaXMpLmh0bWwoKSk7XG4gICAgfSk7XG4gIH0pO1xufSk7XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi4vfi9qc2hpbnQtbG9hZGVyIS4vZXh0cmEvc2VsZWN0LmpzXG4gKiovIiwiLyogZ2xvYmFsIGFuZ3VsYXIgKi9cbid1c2Ugc3RyaWN0JzsgLy8ganNoaW50IGlnbm9yZTpsaW5lXG5cbmV4cG9ydCBkZWZhdWx0IGFuZ3VsYXIubW9kdWxlKCdsdW14LnRleHQtZmllbGQnLCBbXSlcbiAgLmZpbHRlcigndW5zYWZlJywgWyckc2NlJywgZnVuY3Rpb24oJHNjZSlcbiAge1xuICAgIHJldHVybiAkc2NlLnRydXN0QXNIdG1sO1xuICB9XSlcbiAgLmRpcmVjdGl2ZSgnbHhUZXh0RmllbGQnLCBbJyR0aW1lb3V0JywgZnVuY3Rpb24oJHRpbWVvdXQpXG4gIHtcbiAgICByZXR1cm4ge1xuICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgIHNjb3BlOiB7XG4gICAgICAgIGxhYmVsOiAnQCcsXG4gICAgICAgIGRpc2FibGVkOiAnJicsXG4gICAgICAgIGVycm9yOiAnJicsXG4gICAgICAgIHZhbGlkOiAnJicsXG4gICAgICAgIGZpeGVkTGFiZWw6ICcmJyxcbiAgICAgICAgaWNvbjogJ0AnLFxuICAgICAgICB0aGVtZTogJ0AnXG4gICAgICB9LFxuICAgICAgdGVtcGxhdGU6IHJlcXVpcmUoJy4vdGV4dC1maWVsZC5odG1sJyksXG4gICAgICByZXBsYWNlOiB0cnVlLFxuICAgICAgdHJhbnNjbHVkZTogdHJ1ZSxcbiAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtZW50LCBhdHRycywgY3RybCwgdHJhbnNjbHVkZSlcbiAgICAgIHtcbiAgICAgICAgaWYgKGFuZ3VsYXIuaXNVbmRlZmluZWQoc2NvcGUudGhlbWUpKVxuICAgICAgICB7XG4gICAgICAgICAgc2NvcGUudGhlbWUgPSAnbGlnaHQnO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIG1vZGVsQ29udHJvbGxlcixcbiAgICAgICAgICAkZmllbGQ7XG5cbiAgICAgICAgc2NvcGUuZGF0YSA9IHtcbiAgICAgICAgICBmb2N1c2VkOiBmYWxzZSxcbiAgICAgICAgICBtb2RlbDogdW5kZWZpbmVkXG4gICAgICAgIH07XG5cbiAgICAgICAgZnVuY3Rpb24gZm9jdXNVcGRhdGUoKVxuICAgICAgICB7XG4gICAgICAgICAgc2NvcGUuZGF0YS5mb2N1c2VkID0gdHJ1ZTtcbiAgICAgICAgICBzY29wZS4kYXBwbHkoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGJsdXJVcGRhdGUoKVxuICAgICAgICB7XG4gICAgICAgICAgc2NvcGUuZGF0YS5mb2N1c2VkID0gZmFsc2U7XG4gICAgICAgICAgc2NvcGUuJGFwcGx5KCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBtb2RlbFVwZGF0ZSgpXG4gICAgICAgIHtcbiAgICAgICAgICBzY29wZS5kYXRhLm1vZGVsID0gbW9kZWxDb250cm9sbGVyLiRtb2RlbFZhbHVlIHx8ICRmaWVsZC52YWwoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHZhbHVlVXBkYXRlKClcbiAgICAgICAge1xuICAgICAgICAgIG1vZGVsVXBkYXRlKCk7XG4gICAgICAgICAgc2NvcGUuJGFwcGx5KCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiB1cGRhdGVUZXh0YXJlYUhlaWdodCgpXG4gICAgICAgIHtcbiAgICAgICAgICAkdGltZW91dChmdW5jdGlvbigpXG4gICAgICAgICAge1xuICAgICAgICAgICAgJGZpZWxkXG4gICAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdzdHlsZScpXG4gICAgICAgICAgICAgIC5jc3MoeyBoZWlnaHQ6ICRmaWVsZFswXS5zY3JvbGxIZWlnaHQgKyAncHgnIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgdHJhbnNjbHVkZShmdW5jdGlvbigpXG4gICAgICAgIHtcbiAgICAgICAgICAkZmllbGQgPSBlbGVtZW50LmZpbmQoJ3RleHRhcmVhJyk7XG5cbiAgICAgICAgICBpZiAoJGZpZWxkWzBdKVxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHVwZGF0ZVRleHRhcmVhSGVpZ2h0KCk7XG5cbiAgICAgICAgICAgICRmaWVsZC5vbignY3V0IHBhc3RlIGRyb3Aga2V5ZG93bicsIGZ1bmN0aW9uKClcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgdXBkYXRlVGV4dGFyZWFIZWlnaHQoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlXG4gICAgICAgICAge1xuICAgICAgICAgICAgJGZpZWxkID0gZWxlbWVudC5maW5kKCdpbnB1dCcpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgICRmaWVsZC5hZGRDbGFzcygndGV4dC1maWVsZF9faW5wdXQnKTtcbiAgICAgICAgICAkZmllbGQub24oJ2ZvY3VzJywgZm9jdXNVcGRhdGUpO1xuICAgICAgICAgICRmaWVsZC5vbignYmx1cicsIGJsdXJVcGRhdGUpO1xuICAgICAgICAgICRmaWVsZC5vbigncHJvcGVydHljaGFuZ2UgY2hhbmdlIGNsaWNrIGtleXVwIGlucHV0IHBhc3RlJywgdmFsdWVVcGRhdGUpO1xuXG4gICAgICAgICAgbW9kZWxDb250cm9sbGVyID0gJGZpZWxkLmRhdGEoJyRuZ01vZGVsQ29udHJvbGxlcicpO1xuXG4gICAgICAgICAgc2NvcGUuJHdhdGNoKGZ1bmN0aW9uKClcbiAgICAgICAgICB7XG4gICAgICAgICAgICByZXR1cm4gbW9kZWxDb250cm9sbGVyLiRtb2RlbFZhbHVlO1xuICAgICAgICAgIH0sIG1vZGVsVXBkYXRlKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfTtcbiAgfV0pO1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4uL34vanNoaW50LWxvYWRlciEuL2V4dHJhL3RleHQtZmllbGQuanNcbiAqKi8iLCJtb2R1bGUuZXhwb3J0cyA9IFwiPGRpdiBjbGFzcz1cXFwiY2hlY2tib3hcXFwiPlxcbiAgPGlucHV0IG5nLW1vZGVsPVxcXCJtb2RlbFtvcHRpb25zLmtleV1cXFwiXFxuICAgICAgICAgdHlwZT1cXFwiY2hlY2tib3hcXFwiXFxuICAgICAgICAgcm9sZT1cXFwiY2hlY2tib3hcXFwiXFxuICAgICAgICAgY2xhc3M9XFxcImNoZWNrYm94X19pbnB1dFxcXCI+XFxuICA8bGFiZWwgZm9yPXt7OjppZH19IGNsYXNzPVxcXCJjaGVja2JveF9fbGFiZWxcXFwiIGFyaWEtbGFiZWw9XFxcInt7Ojp0by5sYWJlbH19XFxcIj57e3RvLmxhYmVsfX08L2xhYmVsPlxcblxcdDxzcGFuIG5nLWlmPVxcXCI6OnRvLmRlc2NyaXB0aW9uXFxcIlxcbiAgICAgICAgY2xhc3M9XFxcImNoZWNrYm94X19oZWxwXFxcIj57ezo6dG8uZGVzY3JpcHRpb259fTwvc3Bhbj5cXG48L2Rpdj5cIlxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9maWVsZHMvY2hlY2tib3guaHRtbFxuICoqIG1vZHVsZSBpZCA9IDRcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIm1vZHVsZS5leHBvcnRzID0gXCI8bHgtdGV4dC1maWVsZCBtb2RlbD1cXFwiOjptb2RlbFtvcHRpb25zLmtleV1cXFwiXFxuICAgICAgICAgICAgICAgaWNvbj1cXFwie3s6OnRvLmljb259fVxcXCJcXG4gICAgICAgICAgICAgICBmaXhlZC1sYWJlbD1cXFwiOjp0by5maXhlZExhYmVsXFxcIlxcbiAgICAgICAgICAgICAgIHRoZW1lPVxcXCJ7ezo6dG8udGhlbWV9fVxcXCJcXG4gICAgICAgICAgICAgICBkaXNhYmxlZD1cXFwidG8uZGlzYWJsZWRcXFwiXFxuICAgICAgICAgICAgICAgbGFiZWw9XFxcInt7dG8ubGFiZWx9fSB7ezo6dG8ucmVxdWlyZWQgPyAnKicgOiAnJ319XFxcIlxcbiAgICAgICAgICAgICAgIHZhbGlkPVxcXCJmYy4kdmFsaWQgJiYgZmMuJHRvdWNoZWRcXFwiXFxuICAgICAgICAgICAgICAgZXJyb3I9XFxcImZjLiRpbnZhbGlkICYmIGZjLiR0b3VjaGVkXFxcIj5cXG4gIDxpbnB1dCBuZy1tb2RlbD1cXFwibW9kZWxbb3B0aW9ucy5rZXldXFxcIlxcbiAgICAgICAgIHR5cGU9XFxcInt7Ojp0by50eXBlfX1cXFwiXFxuICAgICAgICAgYXJpYS1sYWJlbD1cXFwie3s6OnRvLmxhYmVsfX1cXFwiXFxuICAgICAgICAgbmctY2xhc3M9XFxcIjo6dG8uY2xhc3NOYW1lXFxcIi8+XFxuPC9seC10ZXh0LWZpZWxkPlwiXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2ZpZWxkcy9pbnB1dC5odG1sXG4gKiogbW9kdWxlIGlkID0gNVxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxkaXYgY2xhc3M9XFxcInJhZGlvLWdyb3VwXFxcIj5cXG4gIDxoMyBuZy1pZj1cXFwiOjp0by5sYWJlbFxcXCI+PGxhYmVsPnt7Ojp0by5sYWJlbH19PC9sYWJlbD48L2gzPlxcblxcbiAgPGRpdiBjbGFzcz1cXFwicmFkaW8tYnV0dG9uXFxcIlxcbiAgICAgICBuZy1jbGFzcz1cXFwieydyYWRpby1idXR0b25fX2lubGluZSc6IHRvLmlubGluZX1cXFwiXFxuICAgICAgIG5nLXJlcGVhdD1cXFwibyBpbiB0by5vcHRpb25zXFxcIj5cXG4gICAgPGlucHV0IG5nLW1vZGVsPVxcXCIkcGFyZW50Lm1vZGVsWyRwYXJlbnQub3B0aW9ucy5rZXldXFxcIlxcbiAgICAgICAgICAgaWQ9XFxcInt7OjppZCArICdfJyArICRpbmRleH19XFxcIlxcbiAgICAgICAgICAgdHlwZT1cXFwicmFkaW9cXFwiXFxuICAgICAgICAgICBuZy1kaXNhYmxlZD1cXFwiOjpvLmRpc2FibGVkXFxcIlxcbiAgICAgICAgICAgY2xhc3M9XFxcInJhZGlvLWJ1dHRvbl9faW5wdXRcXFwiXFxuICAgICAgICAgICBuZy12YWx1ZT1cXFwiOjpvLnZhbHVlXFxcIlxcbiAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PVxcXCJ7ezo6aWQgKyAnXycgKyAkaW5kZXggKyAnX3JhZGlvQnV0dG9uJ319XFxcIj5cXG4gICAgPGxhYmVsIGZvcj1cXFwie3s6OmlkICsgJ18nICsgJGluZGV4fX1cXFwiXFxuICAgICAgICAgICBjbGFzcz1cXFwicmFkaW8tYnV0dG9uX19sYWJlbFxcXCI+e3s6Om8ubmFtZX19PC9sYWJlbD5cXG4gICAgICA8c3BhbiBuZy1pZj1cXFwiOjpvLmRlc2NyaXB0aW9uXFxcIiBjbGFzcz1cXFwicmFkaW8tYnV0dG9uX19oZWxwXFxcIj57ezo6by5kZXNjcmlwdGlvbn19XFxuICAgICAgPC9zcGFuPlxcbiAgPC9kaXY+XFxuPC9kaXY+XFxuXCJcblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZmllbGRzL3JhZGlvLmh0bWxcbiAqKiBtb2R1bGUgaWQgPSA2XG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCJtb2R1bGUuZXhwb3J0cyA9IFwiPGRpdiBjbGFzcz1cXFwiZHJvcGRvd25cXFwiPlxcbiAgPGRpdiBjbGFzcz1cXFwiZHJvcGRvd24tY29udGFpbmVyXFxcIj5cXG4gICAgPHAgY2xhc3M9XFxcImRyb3Bkb3duLWRlc2NyaXB0aW9uXFxcIj57ezo6dG8ubGFiZWx9fTwvcD5cXG4gICAgPHAgY2xhc3M9XFxcImRyb3Bkb3duLWJ1dHRvblxcXCI+Q2xpY2sgdG8gU2VsZWN0PC9wPlxcbiAgICA8dWwgY2xhc3M9XFxcImRyb3Bkb3duLW1lbnUgZHJvcGRvd24tc2VsZWN0XFxcIj5cXG4gICAgICA8bGkgbmctcmVwZWF0PVxcXCJvcHRpb24gaW4gdG8ub3B0aW9uc1xcXCI+e3s6Om9wdGlvbn19PC9saT5cXG4gICAgPC91bD5cXG4gIDwvZGl2PlxcbjwvZGl2PlwiXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2ZpZWxkcy9zZWxlY3QuaHRtbFxuICoqIG1vZHVsZSBpZCA9IDdcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIm1vZHVsZS5leHBvcnRzID0gXCI8bGFiZWwgY2xhc3M9XFxcImxhYmVsLXN3aXRjaFxcXCI+XFxuICA8aW5wdXQgdHlwZT1cXFwiY2hlY2tib3hcXFwiXFxuICAgICAgICAgcm9sZT1cXFwiY2hlY2tib3hcXFwiXFxuICAgICAgICAgbmctbW9kZWw9XFxcIm1vZGVsW29wdGlvbnMua2V5XVxcXCIvPlxcbiAgPGRpdiBjbGFzcz1cXFwiY2hlY2tib3hcXFwiPjwvZGl2PlxcbiAge3t0by5sYWJlbH19XFxuICA8c3BhbiBuZy1pZj1cXFwiOjp0by5kZXNjcmlwdGlvblxcXCJcXG4gICAgICAgIGNsYXNzPVxcXCJjaGVja2JveF9faGVscFxcXCI+e3s6OnRvLmRlc2NyaXB0aW9ufX08L3NwYW4+XFxuPC9sYWJlbD5cIlxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9maWVsZHMvc3dpdGNoLmh0bWxcbiAqKiBtb2R1bGUgaWQgPSA4XG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCJtb2R1bGUuZXhwb3J0cyA9IFwiPGx4LXRleHQtZmllbGQgbW9kZWw9XFxcIjo6bW9kZWxbb3B0aW9ucy5rZXldXFxcIlxcbiAgICAgICAgICAgICAgIGZpeGVkLWxhYmVsPVxcXCI6OnRvLmZpeGVkTGFiZWxcXFwiXFxuICAgICAgICAgICAgICAgaWNvbj1cXFwie3s6OnRvLmljb259fVxcXCJcXG4gICAgICAgICAgICAgICB0aGVtZT1cXFwie3s6OnRvLnRoZW1lfX1cXFwiXFxuICAgICAgICAgICAgICAgbGFiZWw9XFxcInt7dG8ubGFiZWx9fSB7ezo6dG8ucmVxdWlyZWQgPyAnKicgOiAnJ319XFxcIlxcbiAgICAgICAgICAgICAgIHZhbGlkPVxcXCJmYy4kdmFsaWQgJiYgZmMuJHRvdWNoZWRcXFwiXFxuICAgICAgICAgICAgICAgZXJyb3I9XFxcImZjLiRpbnZhbGlkICYmIGZjLiR0b3VjaGVkXFxcIj5cXG4gICAgPHRleHRhcmVhIG5nLW1vZGVsPVxcXCJtb2RlbFtvcHRpb25zLmtleV1cXFwiXFxuICAgICAgICAgICAgICBhcmlhLWxhYmVsPVxcXCJ7ezo6dG8ubGFiZWx9fVxcXCJcXG4gICAgICAgICAgICAgIHJvd3M9XFxcInt7Ojp0by5yb3dzfX1cXFwiIGNvbHM9XFxcInt7Ojp0by5jb2xzfX1cXFwiPlxcbiAgICA8L3RleHRhcmVhPlxcbjwvbHgtdGV4dC1maWVsZD5cXG5cIlxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9maWVsZHMvdGV4dGFyZWEuaHRtbFxuICoqIG1vZHVsZSBpZCA9IDlcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIm1vZHVsZS5leHBvcnRzID0gXCI8ZGl2IGNsYXNzPVxcXCJ0ZXh0LWZpZWxkIHRleHQtZmllbGQtLXt7IHRoZW1lIH19LXRoZW1lXFxcIlxcbiAgICAgbmctY2xhc3M9XFxcInsgJ3RleHQtZmllbGQtLWlzLXZhbGlkJzogdmFsaWQoKSxcXG4gICAgICAgICAgICAgICAgICd0ZXh0LWZpZWxkLS1oYXMtZXJyb3InOiBlcnJvcigpLFxcbiAgICAgICAgICAgICAgICAgJ3RleHQtZmllbGQtLWlzLWRpc2FibGVkJzogZGlzYWJsZWQoKSxcXG4gICAgICAgICAgICAgICAgICd0ZXh0LWZpZWxkLS1maXhlZC1sYWJlbCc6IGZpeGVkTGFiZWwoKSxcXG4gICAgICAgICAgICAgICAgICd0ZXh0LWZpZWxkLS1pcy1hY3RpdmUnOiBkYXRhLm1vZGVsIHx8IGRhdGEuZm9jdXNlZCxcXG4gICAgICAgICAgICAgICAgICd0ZXh0LWZpZWxkLS1pcy1mb2N1c2VkJzogZGF0YS5mb2N1c2VkLFxcbiAgICAgICAgICAgICAgICAgJ3RleHQtZmllbGQtLWxhYmVsLWhpZGRlbic6IGZpeGVkTGFiZWwoKSAmJiBkYXRhLm1vZGVsLFxcbiAgICAgICAgICAgICAgICAgJ3RleHQtZmllbGQtLXdpdGgtaWNvbic6IGljb24gJiYgZml4ZWRMYWJlbCgpIH1cXFwiPlxcbiAgPGxhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZpZWxkX19sYWJlbFxcXCIgbmctYmluZC1odG1sPVxcXCJsYWJlbCB8IHVuc2FmZVxcXCI+PC9sYWJlbD5cXG5cXG4gIDxkaXYgY2xhc3M9XFxcInRleHQtZmllbGRfX2ljb25cXFwiIG5nLWlmPVxcXCJpY29uICYmIGZpeGVkTGFiZWwoKSBcXFwiPlxcbiAgICA8aSBjbGFzcz1cXFwibWRpIG1kaS17eyBpY29uIH19XFxcIj48L2k+XFxuICA8L2Rpdj5cXG5cXG4gIDxkaXYgbmctdHJhbnNjbHVkZT1cXFwiMVxcXCI+PC9kaXY+XFxuPC9kaXY+XCJcblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZXh0cmEvdGV4dC1maWVsZC5odG1sXG4gKiogbW9kdWxlIGlkID0gMTBcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyJdLCJzb3VyY2VSb290IjoiIn0=