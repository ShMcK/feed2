angular.module("app").run(["$rootScope", "$location", function ($rootScope, $location) {
  $rootScope.$on("$stateChangeError", function (event, next, previous, error) {
    // We can catch the error thrown when the $requireUser promise is rejected
    // and redirect the user back to the main page
    if (error === "AUTH_REQUIRED") {
      $location.path("/products");
    }
  });
}]);

angular.module("app").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function ($urlRouterProvider, $stateProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('products', {
        url: '/products',
        templateUrl: 'client/components/product-list/product-list.ng.html',
        controller: 'ProductListCtrl',
        controllerAs: 'productList'
      })
      .state('product', {
        url: '/products/:productId',
        templateUrl: 'client/components/product-details/product-details.ng.html',
        controller: 'ProductDetailsCtrl',
        controllerAs: 'productDetails' //,
        //resolve: {
        //'currentUser': ["$meteor", function($meteor){
        //  return $meteor.requireUser();
        //}],
        //'subscribe': ['$meteor', function ($meteor) {
        //  return $meteor.subscribe('products');
        //}]
        //}
      })
      .state('addProduct', {
        url: '/admin/addProduct',
        templateUrl: 'client/components/product-add/product-add.ng.html',
        controller: 'AddProductCtrl',
        controllerAs: 'add'
      });

    $urlRouterProvider.otherwise("/products");
  }]);