angular.module('app')
  .filter('fromNow', fromNow);

function fromNow() {
  return function (date) {
    return moment.utc(date).fromNow();
  };
}